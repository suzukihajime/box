box -- Easy Box Generator for Laser Cutting Machines
==========

moved to [https://github.com/ocxtal/box.py](https://github.com/ocxtal/box.py)

Description
----------
This is a python script to generat a box  blueprint suitable for the Laser cutting machines.


Usage
----------
./box.py <width> <height> <depth> <thickness> <pitch> <output filename>

